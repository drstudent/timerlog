<?php

namespace Tnedutsrd\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;

class TnedutsrdExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $container)
    {
	    $configuration = new Configuration();
	    $config = $this->processConfiguration($configuration, $configs);

	    $definition = $container->getDefinition('tnedutsrd.generator.stuff_generator');
	    $definition->replaceArgument(0, 'v00new');
    }
}
