package main

import (
	"log"
	"time"
)

func main() {
	log.SetFlags(log.LstdFlags | log.Lshortfile)

	for {
		log.Println(time.Now())
		time.Sleep(time.Minute)
	}
}
